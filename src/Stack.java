
public class Stack<T> {
	
class Node<T> {
		
		T data;
		Node<T> next;

	}
	
	Node<T> node;
	
	public Stack() {
		node = null;
		
	}
	
	public void push(T data) {
				
		Node<T> temp = new Node<>();
		temp.data = data;
		temp.next = node;
		node = temp;
		
	}
	
	public boolean isEmpty() {
		return node == null;
				
	}
	
	public int size() {
		return isEmpty() ? 0 : size(node);
	}
	
	private final int size(Node<T> node) {
		
		return node.next == null ? 1 : (1 + size(node.next));
				
	}
	
	public T peak() {
		return isEmpty() ? null : node.data;
	}
	
	public T pop() {
		
		if(isEmpty()) 
			return null;
		
		T temp = node.data;
		node = node.next;
		return temp;
		
	}

}
